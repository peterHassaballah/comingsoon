import React from 'react';
import { ReactComponent as IconFacebook } from './assets/icons/facebook.svg';
// import { ReactComponent as IconTwitter } from './assets/icons/twitter.svg';
import "./App.css";

class App extends React.Component {
  render = () => {
    return (
      <div className="card">
        <div className="header">
          <div className="logo"> 
          <img
            width="70"
                  src="https://i.ibb.co/g9SVhyw/Logo-01.png" alt='logo'
                />
          </div>
          <div className="social">
            <a href="https://www.facebook.com/solis.eg.3" title="Facebook" target="_blank" rel="noopener noreferrer">
              <IconFacebook className="icon" />
            </a>
            {/* <a href="https://twitter.com" title="Twitter" target="_blank" rel="noopener noreferrer">
              <IconTwitter className="icon" />
            </a> */}
            
          </div>
        </div>
        <div className="content">
          <div className="title-holder">
            <h1>Solid Security Services</h1>
            <p>Website coming soon. Please check back to know more. Send us an email if you have any questions.</p>
          </div>
          <a href="mailto:info@solidgroupeg.com">
            <div className="cta">Send us an email</div>
          </a>
        </div>
        <div className="footer">
          <span>Developed by Out Of The Box Solutions </span>
        </div>
      </div>
    );
  }
}

export default App;
